﻿using System;
using System.Collections.Generic;

namespace Rdvb.Tg.Graph.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Node> nodes = new List<Node>
            {
                new Node<string>("Node-0"),
                new Node<string>("Node-1"),
                new Node<string>("Node-2"),
                new Node<string>("Node-3"),
                new Node<string>("Node-4"),
                new Node<string>("Node-5")
            };

            List<Edge> edges = new List<Edge>
            {
                new Edge(nodes[0], nodes[1]),
                new Edge(nodes[0], nodes[2]),
                new Edge(nodes[1], nodes[3]),
                new Edge(nodes[1], nodes[4]),
                new Edge(nodes[4], nodes[5])
            };

            Graph graph = new Graph(edges, nodes);

            Console.WriteLine("Inital graph:");
            Console.WriteLine("Nodes:");
            graph.Nodes.ToConsole();
            Console.WriteLine("Edges:");
            graph.Edges.ToConsole();
            Console.WriteLine("Inbound edges of {0}:", graph.Nodes[5]);
            graph.Nodes[5].InboundEdges.ToConsole();
            Console.WriteLine("Outbound edges of {0}:", graph.Nodes[1]);
            graph.Nodes[1].OutboundEdges.ToConsole();

            Console.WriteLine("Adding new node and edge:");
            graph.AddNode(new Node<string>("Node-6"));
            graph.AddEdge(graph.Nodes[6], graph.Nodes[5]);
            Console.WriteLine("Nodes:");
            graph.Nodes.ToConsole();
            Console.WriteLine("Edges:");
            graph.Edges.ToConsole();
            Console.WriteLine("Inbound edges of {0}:", graph.Nodes[5]);
            graph.Nodes[5].InboundEdges.ToConsole();
            Console.WriteLine("Outbound edges of {0}:", graph.Nodes[1]);
            graph.Nodes[1].OutboundEdges.ToConsole();

            Console.WriteLine("Removing node {0}:", graph.Nodes[1]);
            graph.RemoveNode(graph.Nodes[1]);
            Console.WriteLine("Nodes:");
            graph.Nodes.ToConsole();
            Console.WriteLine("Edges:");
            graph.Edges.ToConsole();
            Console.WriteLine("Inbound edges of {0}:", graph.Nodes[5]);
            graph.Nodes[5].InboundEdges.ToConsole();
            Console.WriteLine("Outbound edges of {0}:", graph.Nodes[1]);
            graph.Nodes[1].OutboundEdges.ToConsole();

            Console.WriteLine("Done.");
            Console.ReadLine();
        }

        
    }

    public static class EnumerableHelper
    {
        public static void ToConsole<T>(this IEnumerable<T> enumerable)
        {
            foreach (T item in enumerable)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("");
        }
    }

}
