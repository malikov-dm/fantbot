﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Extensions;
using Rdvb.Tg.Fant.Library.Factories;
using Rdvb.Tg.Fant.Library.Shared;
using System;
using System.Globalization;
using System.Threading;

namespace Rdvb.Tg.Fant.Client
{
    public class ConsoleClient
    {
        public static void Subscribe()
        {

        }
        public static void Create(string fileName)
        {
#if DEBUG
            Console.WriteLine("Конфигурация DEBUG");
#else
            Console.WriteLine("Конфигурация RELEASE");
#endif
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            var world = GameWorldFactory.CreateGameWorld(fileName);
            world.NotificationEvent += World_NotificationEvent;
            //var world = GameWorldFactory.CreateGameWorld(fileName);

            Console.WriteLine(world.LastWriteTime);

            var player = PlayerFactory.CreatePlayer("ПЕРВЫЙ ИГРОК", str: 10, dex: 8, specialAbility: SpecialAbility.SecretHit);
            

            world.Player = player;

            var session = GameSessionFactory.CreateGameSession(player);
            world.GameSessions.Add(session);

            var section = world.GetSectionById(1);
            player.Section = section;
            player.Section.MoveNext(section);
            //player.Section = section;

            player.StatsStrings.ToConsole();
            player.Inventory.ToConsole();
            Console.WriteLine(player.QuestsString);
            player.Inventory.PistolsToConsole();

            Console.WriteLine();
            Console.WriteLine(player.Section.Title);
            player.Section.Body.ToConsole();
            player.Section.Enemies.ToConsole();
            player.Section.Links.Numerate().ToConsole();

            Console.WriteLine();

            bool alive = true;
            while (alive)
            {

                try
                {
                    string linkClientId = Console.ReadLine();

                    section = player.Section.MoveNext(linkClientId);


                    player.StatsStrings.ToConsole();
                    player.Inventory.ToConsole();
                    Console.WriteLine(player.QuestsString);
                    player.Inventory.PistolsToConsole();

                    Console.WriteLine();
                    Console.WriteLine(player.Section.Title);
                    player.Section.Body.ToConsole();
                    player.Section.Enemies.ToConsole();
                    player.Section.Links.Numerate().ToConsole();

                    Console.WriteLine();
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            }
            Console.ReadLine();
        }

        private static void World_NotificationEvent(object sender, NotificationEventArgs e)
        {
            Console.WriteLine($"Console Client: {e.Message}");
        }
    }
}
