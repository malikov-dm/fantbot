﻿using Rdvb.Tg.Fant.Library.Analyzer;
using Rdvb.Tg.Fant.Library.Builders;
using System;

namespace Rdvb.Tg.Fant.Converter
{
    class Program
    {
        static void Main(string[] args)
        {
            //var gb = new GraphBuilder();
            //gb.Run("input\\book.xml");

            var jpb = new JsonProxyBuilder();
            jpb.Run(@"input\book.xml", @"input\book.json");

            //var gw = GameWorldBuilder.LoadJson(@"input\book.json");

            Console.ReadLine();
        }
    }
}
