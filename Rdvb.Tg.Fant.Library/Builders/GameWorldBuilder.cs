﻿using Newtonsoft.Json;
using Rdvb.Tg.Fant.Library;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Rdvb.Tg.Fant.Library.Builders;

namespace Rdvb.Tg.Fant.Library.Builders
{
    public static class GameWorldBuilder
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static GameWorld LoadJson(string inputFilePath)
        {
            var json = File.ReadAllText(inputFilePath);
            var gw = JsonConvert.DeserializeObject<GameWorld>(json);
            gw.LastWriteTime = File.GetLastWriteTime(inputFilePath);
            logger.Info($"GameWorldBuilder.LoadJson :: {gw.LastWriteTime}");
            return gw;
        }

        public static GameWorld LoadSessions(this GameWorld gameWorld)
        {
            return gameWorld;
        }

        //public static GameWorld Subscribe(this GameWorld gameWorld)
        //{
        //    gameWorld.NotificationEvent += GameWorld_NotificationEvent;
        //    return gameWorld;
        //}

        //private static void GameWorld_NotificationEvent(object sender, NotificationEventArgs e)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
