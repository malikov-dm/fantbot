﻿using Rdvb.Tg.Graph;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using Newtonsoft.Json;
using System.IO;

namespace Rdvb.Tg.Fant.Library.Analyzer
{

    public class GraphBuilder
    {
        List<Node> nodes = new List<Node>();
        List<Edge> edges = new List<Edge>();

        public void Run(string filePath)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding.GetEncoding("windows-1254");

            var doc = new XmlDocument();
            doc.Load(filePath);
            var root = doc.DocumentElement;
            var nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ns", doc.DocumentElement.NamespaceURI);

            var sections = root.SelectNodes("//ns:section[@id]", nsmgr);

            foreach (XmlNode sect in sections)
            {
                if (!Int32.TryParse(sect.SelectSingleNode("ns:title/ns:p", nsmgr).InnerText, out int intId))
                {
                    continue;
                }
                var strId = sect.SelectSingleNode("@id").Value;

                nodes.Add(new ExtNode<int, string>(intId, strId));
            }

            foreach (XmlNode sect in sections)
            {
                if (!Int32.TryParse(sect.SelectSingleNode("ns:title/ns:p", nsmgr).InnerText, out int intId))
                {
                    continue;
                }

                var strId = sect.SelectSingleNode("@id").Value;
                var fromNode = nodes.Cast<ExtNode<int, string>>().FirstOrDefault(x => x.Payload == strId);

                var linkNodes = sect.SelectNodes("ns:p/ns:a", nsmgr);
                foreach (XmlNode link in linkNodes)
                {

                    var value = Convert.ToInt32(link.InnerText);
                    var toNode = nodes.Cast<ExtNode<int, string>>().FirstOrDefault(x => x.NodeId == value);
                    edges.Add(new Edge(fromNode, toNode));

                }

                var hiddenLinkNodes = sect.SelectNodes("ns:hidden/ns:item", nsmgr);
                foreach(XmlNode n in hiddenLinkNodes)
                {
                    var value = intId + Convert.ToInt32(n.InnerText);
                    var toNode = nodes.Cast<ExtNode<int, string>>().FirstOrDefault(x => x.NodeId == value);
                    edges.Add(new Edge(fromNode, toNode));
                }
            }

            Rdvb.Tg.Graph.Graph graph = new Rdvb.Tg.Graph.Graph(edges, nodes);

            Console.WriteLine($"Вершин в графе: {nodes.Count}");
            Console.WriteLine($"Ребер в графе: {edges.Count}");

            var noInboundEdgesNodes = graph.Nodes.Where(x => x.InboundEdges.Count == 0);
            Console.WriteLine($"Количество вершин без входящих путей: {noInboundEdgesNodes.Count()}");
            noInboundEdgesNodes.ToConsole();

            var noOutboundEdgesNodes = graph.Nodes.Where(x => x.OutboundEdges.Count == 0);
            Console.WriteLine($"Количество вершин без исходящих путей: {noOutboundEdgesNodes.Count()}");
            noOutboundEdgesNodes.ToConsole();

        }

    }

    public static class EnumerableHelper
    {
        public static void ToConsole<T>(this IEnumerable<T> enumerable)
        {
            foreach (T item in enumerable)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
        }
    }

    internal class ExtNode<TNum, TString> : Node<TString>
    {
        public TNum NodeId { get; private set; }
        public ExtNode(TNum id, TString payload) : base(payload)
        {
            NodeId = id;
        }
    }
}
