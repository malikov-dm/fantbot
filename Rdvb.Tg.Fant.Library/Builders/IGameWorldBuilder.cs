﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Builders
{
    interface IGameWorldBuilder
    {
        GameWorld LoadJson(string inputFilePath);
        GameWorld LoadSessions();
    }
}
