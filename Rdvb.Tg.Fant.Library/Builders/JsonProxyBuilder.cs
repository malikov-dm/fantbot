﻿using Newtonsoft.Json;
using Rdvb.Tg.Fant.Library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Rdvb.Tg.Fant.Library.Analyzer
{
    public class JsonProxyBuilder
    {


        public void Run(string inputFilePath, string outputFilePath)
        {
            GameWorld fb = new GameWorld();

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding.GetEncoding("windows-1254");

            var doc = new XmlDocument();
            doc.Load(inputFilePath);
            var root = doc.DocumentElement;
            var nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ns", doc.DocumentElement.NamespaceURI);

            var nodes = root.SelectNodes("//ns:body/ns:section", nsmgr);

            foreach (XmlNode n in nodes)
            {
                var nid = n.SelectSingleNode("@id").Value;
                var ntitle = n.SelectSingleNode("ns:title/ns:p", nsmgr).InnerText;
                var nbody = n.SelectNodes("ns:p", nsmgr);
                var nlinks = n.SelectNodes("ns:p/ns:a", nsmgr);
                var nhlinks = n.SelectNodes("ns:hidden/ns:item", nsmgr);
                var nsections = n.SelectNodes("ns:section", nsmgr);

                var section = new Section(nid, ntitle);

                ParseSection(n, nsmgr, section);

                var sectionAttributes = ParseAttributes(n, nsmgr);
                section.SectionAttributes = sectionAttributes;

                ParseLinks(nlinks, nsmgr, section);
                ParseHiddenLinks(nhlinks, nsmgr, section);

                foreach (XmlNode ns in nsections)
                {
                    var nsid = ns.SelectSingleNode("@id")?.Value;
                    var nstitle = ns.SelectSingleNode("ns:title/ns:p", nsmgr)?.InnerText;
                    var nsbody = ns.SelectNodes("ns:p", nsmgr);

                    var subsection = new Section(nsid, nstitle);

                    foreach (XmlNode nb in nsbody)
                    {
                        var str = nb.InnerText;
                        subsection.Body.Add(str);
                    }

                    section.Sections.Add(subsection);
                }

                foreach (XmlNode nb in nbody)
                {
                    var str = nb.InnerText;
                    section.Body.Add(str);
                }



                ParseEnemies(n, nsmgr, section);

                fb.Sections.Add(section);
            }

            //var json = JsonConvert.SerializeObject(fb, Newtonsoft.Json.Formatting.Indented);
            var file = new System.IO.FileInfo(outputFilePath);
            file.Directory.Create();
            using (StreamWriter sw = File.CreateText(outputFilePath))
            {
                JsonSerializer serializer = new JsonSerializer
                {
                    Formatting = Newtonsoft.Json.Formatting.Indented,
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore
                };
                serializer.Serialize(sw, fb);
            }

            Console.WriteLine("Сериализация завершена");
        }


        internal void ParseEnemies(XmlNode node, XmlNamespaceManager nsmgr, Section section)
        {
            var enodes = node.SelectNodes("ns:cite/ns:p", nsmgr);

            for (int i = 0; i < enodes.Count; i += 2)
            {
                var enemy = new Enemy();
                var n = enodes[i];
                enemy.MaxRoundCount = Convert.ToInt32(ParseAttributeValue("round", n, nsmgr));
                enemy.Name = n?.InnerText;
                var tarr = enodes[i + 1]?.InnerText.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                enemy.Dex = Convert.ToInt32(tarr[1]);
                enemy.Str = Convert.ToInt32(tarr[3]);
                enemy.MinStr = Convert.ToInt32(ParseAttributeValue("defeated", n, nsmgr));
                section.Enemies.Add(enemy);
            }
        }
        internal string ParseAttributeValue(string name, XmlNode node, XmlNamespaceManager nsmgr)
        {
            var attrs = node.Attributes;
            foreach (XmlAttribute attr in attrs)
            {
                if (attr.Name.ToLower() == name)
                {
                    return attr.Value;
                }
            }
            return null;
        }

        internal Dictionary<string, string> ParseAttributes(XmlNode node, XmlNamespaceManager nsmgr)
        {
            var attrs = node.Attributes;
            var res = new Dictionary<string, string>();
            foreach (XmlAttribute attr in attrs)
            {
                if (attr.Name.ToLower() == "id" || attr.Name.ToLower() == "l:href")
                {
                    continue;
                }
                res.Add(attr.Name, attr.Value);
            }
            return res;
        }
        internal void ParseLinks(XmlNodeList nodes, XmlNamespaceManager nsmgr, Section section)
        {
            foreach (XmlNode nl in nodes)
            {

                var val = nl.InnerText;
                var linkAttributes = ParseAttributes(nl, nsmgr);
                section.Links.Add(new Link($"n_{val}", val) { LinkAttributes = linkAttributes });
            }
        }
        internal void ParseHiddenLinks(XmlNodeList nodes, XmlNamespaceManager nsmgr, Section section)
        {
            // hidden links
            foreach (XmlNode nhl in nodes)
            {
                var val = section.Id + Convert.ToInt32(nhl.InnerText);
                section.Links.Add(new Link($"n_{val}", val.ToString(), true));
            }
        }
        internal void ParseSection(XmlNode node, XmlNamespaceManager nsmgr, Section section)
        {
            //var val = node.Attributes["str"]?.Value;
            //if (!string.IsNullOrEmpty(val) && Int32.TryParse(val, out int str))
            //{
            //    section.Str = str;
            //}
        }
    }
}
