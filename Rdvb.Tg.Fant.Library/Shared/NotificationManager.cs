﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Shared
{
    public class NotificationManager
    {
        protected internal event NotificationEventHandler NotificationEvent;

        protected internal void SendNotification(NotificationEventArgs e, NotificationEventHandler handler)
        {
            if (handler != null && e != null)
                handler(this, e);
        }

        public void SendNotification(NotificationEventArgs e)
        {
            SendNotification(e, NotificationEvent);
        }
    }
}
