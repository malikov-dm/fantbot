﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Shared
{
    public class Dice
    {

        protected internal event DiceEventHandler Throwed;

        private readonly Random _rnd = new Random();

        public Dice()
        {
            Throwed += Dice_Throwed;
        }

        private void Dice_Throwed(object sender, DiceEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }

        protected internal void CallEvent(DiceEventArgs e, DiceEventHandler handler)
        {
            if (handler != null && e != null)
                handler(this, e);
        }

        protected virtual void OnThrowed(DiceEventArgs e)
        {
            CallEvent(e, Throwed);
        }

        public int Throw(int minimumValue, int maximumValue)
        {
            var res = _rnd.Next(minimumValue, maximumValue + 1);
            OnThrowed(new DiceEventArgs($"Бросаю кубик... {res}"));
            return res;
        }
        public int Throw6()
        {
            var res = _rnd.Next(1, 7);
            OnThrowed(new DiceEventArgs($"Бросаю кубик... {res}"));
            return res;
        }

        public bool CheckLuck()
        {
            var res = _rnd.Next() % 2 == 0;
            var mes = res ? "Повезло!" : "Не повезло...";
            OnThrowed(new DiceEventArgs($"Бросаю кубик на удачу... {mes}"));
            return res;
        }
    }
}
