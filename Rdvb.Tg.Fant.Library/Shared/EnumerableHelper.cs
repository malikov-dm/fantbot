﻿using Rdvb.Tg.Fant.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Shared
{
    public static class EnumerableHelper
    {
        public static void ToConsole<T>(this IEnumerable<T> enumerable)
        {
            foreach (T item in enumerable)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("");
        }

        //public static void ToConsole<T>(this IPrintable<T> t)
        //{
        //    t.ToConsole();
        //    Console.WriteLine(t.ToString());
        //    Console.WriteLine("");
        //}
        public static void ToConsole(this IPrintable t)
        {
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine(t.ToString());
            //Console.WriteLine();
        }


        public static void AddDist(this IList<Link> list, Link item)
        {
            if (item == null) return;

            if (!list.Any(x => x.Id == item.Id))
            {
                list.Add(item);
            }
        }

        public static void AddSafely(this IList<Link> list, Link item)
        {
            if (item == null) return;

            list.Add(item);

        }
        public static IList<Link> Numerate(this IList<Link> list)
        {
            for (int i = 1; i <= list.Count; i++)
            {
                list[i - 1].ClientId = $"{i}";
            }

            return list;
        }
    }
}
