﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Shared
{
    public class GenGameLinks
    {
        public static Link GetFoodForMoneyLink(Player player, int targetSectionId, decimal money, int str)
        {
            // если денег не хватает, не рисуем ссылку
            if (player.Inventory.Money < Math.Abs(money)) return null;

            //ели сегодня уже ел, не рисуем ссылку
            if (player.Full) return null;


            var link = new Link()
            {
                Id = targetSectionId,
                Title = $"{targetSectionId}",
                Description = $"Восстановить {str} STR за {money} Money",
                TargetSectionId = $"n_{targetSectionId}",
                LinkAttributes = new Dictionary<string, string>()
                {
                    { "str", $"{str}" },
                    { "money", $"{money}" }

                },
                LinkType = LinkType.GameLink,
                LinkEvent = LinkEvent.EatUp
            };
            return link;
        }

        public static Link GetInventoryFoodLink(Player player, int targetSectionId, int str)
        {
            if (player.Inventory.Food == 0 && !player.Full)
            {
                player.Stats.Str += str;
                return null;
            }

            if (player.Full) return null;

            var link = new Link()
            {
                Id = targetSectionId,
                Title = $"{targetSectionId}",
                Description = $"Съесть часть припасов, иначе будет потеряно {str} STR ",
                TargetSectionId = $"n_{targetSectionId}",
                LinkAttributes = new Dictionary<string, string>()
                {
                    { "food", "-1" }

                },
                LinkType = LinkType.GameLink,
                LinkEvent = LinkEvent.EatUp
            };
            return link;
        }

        internal static Link GetDoNothingLink(Player player, int targetSectionId)
        {
            var link = new Link()
            {
                Id = targetSectionId,
                Title = $"{targetSectionId}",
                Description = $"Ничего не делать",
                TargetSectionId = $"n_{targetSectionId}",
                LinkType = LinkType.GameLink,
                LinkEvent = LinkEvent.DoNothing
            };
            return link;
        }

        internal static Link GetPistolLink(Player player, int targetSectionId)
        {
            var link = new Link()
            {
                Id = targetSectionId,
                Title = $"{targetSectionId}",
                Description = $"Выстрелить из пистолета наверняка (противник получит -4 STR)",
                TargetSectionId = $"n_{targetSectionId}",
                LinkType = LinkType.GameLink,
                LinkEvent = LinkEvent.Shoot
            };
            return link;
        }
        internal static Link GetRandomPistolLink(Player player, int targetSectionId)
        {
            var link = new Link()
            {
                Id = targetSectionId,
                Title = $"{targetSectionId}",
                Description = $"Выстрелить из пистолета наудачу (в случае удачи противник погибнет)",
                TargetSectionId = $"n_{targetSectionId}",
                LinkType = LinkType.GameLink,
                LinkEvent = LinkEvent.RandomShot
            };
            return link;
        }
    }
}
