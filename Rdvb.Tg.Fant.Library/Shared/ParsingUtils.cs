﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Threading;

namespace Rdvb.Tg.Fant.Library.Shared
{
    public static class ParsingUtils
    {
        static ParsingUtils()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }

        public static T Parse<T>(this Dictionary<string, string> dic, string key)
        {
            if (dic == null || !dic.TryGetValue(key, out string val))
            {
                return default(T);
            }

            return val.Convert<T>();

        }

        public static bool TryParse<T>(this Dictionary<string, string> dic, string key, out T res)
        {
            if (dic == null)
            {
                res =  default(T);
                return false;
            }
            if (dic.TryGetValue(key, out string val)) {
                res = val.Convert<T>();
                return true;
            }
            res = default(T);
            return false;

        }
        public static bool TryParse<T>(this Dictionary<string, string> dic, string key)
        {
            return TryParse<T>(dic, key, out T res);
        }
        public static T Convert<T>(this string input)
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                if (converter != null)
                {
                    // Cast ConvertFromString(string text) : object to (T)
                    return (T)converter.ConvertFromString(input);
                }
                return default(T);
            }
            catch (NotSupportedException)
            {
                return default(T);
            }
        }
    }
}
