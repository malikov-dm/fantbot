﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Shared
{
    public class GenGameSections
    {
        public static Section NoSwimmingAbilitySection(SectionEventArgs e)
        {
            var sect = new GameSection(e)
            {
                SectionId = "noswimming",
                Title = "Какая забывчивость!",
                Body = new List<string> {
                                "Вообще-то, плавать вы не умеете. Умение плавать можно было выбрать в начале игры."
                                //,"Вернуться к выбору действия или начать с начала?"
                            }
            };

            return sect;
        }

        public static Section NoTwoHandedAbilitySection(SectionEventArgs e)
        {
            var sect = new GameSection(e)
            {
                SectionId = "notwohanded",
                Title = "Какая забывчивость!",
                Body = new List<string> {
                                "Вообще-то, стрелять сразу из двух пистолетов вы не умеете. Такое умение можно было выбрать в начале игры."
                                //,"Вернуться к выбору действия или начать с начала?"
                            }
            };

            return sect;
        }

        public static Section NoMoreFaithSection(SectionEventArgs e)
        {
            var sect = new GameSection(e)
            {
                SectionId = "nomorefaith",
                Title = "Какая забывчивость!",
                Body = new List<string> {
                                "Вы возносите горячую молитву к небесам, но вас никто не слышит.",
                                "Вероятно, вы использовали свой шанс обратиться к Богу или не были не слишком ревностным протестантом..."
                    //"Вернуться к выбору действия или начать с начала?"
                }
            };

            return sect;
        }

        public static Section GameOverByStrSection()
        {
            var sect = new GameSection()
            {
                SectionId = "nomorestr",
                Title = "GAME OVER",
                Body = new List<string> {
                                "Ваши жизненные силы иссякли и вы погибли."
                },
                SectionType = SectionType.GameOver
            };

            return sect;
        }

        //public static Section RandomPistolShootSection(SectionEventArgs e, bool luck)
        //{
        //    var sect = new GameSection(e)
        //    {
        //        SectionId = "nomorefaith",
        //        Title = "Какая забывчивость!",
        //        Body = new List<string> {
        //                        "Вы стреляете в противника из своего пистолета наудачу.",
        //                        luck ? "С такого расстояния никто не смог бы промахнуться." : "К сожалению, вы промахнулись..."
        //            //"Вернуться к выбору действия или начать с начала?"
        //        }
        //    };

        //    return sect;
        //}

        //public static Section PistolShootSection(SectionEventArgs e)
        //{
        //    var sect = new GameSection(e)
        //    {
        //        SectionId = "pistol_shoot_sect",
        //        Title = "Выстрел из пистолета наверняка!",
        //        Body = new List<string> {
        //                        "Вы стреляете в противника из своего пистолета наверняка.",
        //                        "С такого расстояния никто не смог бы промахнуться."
        //            //"Вернуться к выбору действия или начать с начала?"
        //        }
        //    };

        //    return sect;
        //}


    }
}
