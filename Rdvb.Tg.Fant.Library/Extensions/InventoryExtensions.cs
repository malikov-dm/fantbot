﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Extensions
{
    public static class InventoryExtensions
    {
        public static void AddFood(this Inventory inventory, int val)
        {
            if (inventory is Inventory<InventoryItem> inv)
            {
                if (val >= 0)
                {
                    for (int i = 0; i < val; i++) inv.Add(InventoryItem.Create("Еда", InventoryItemType.ConsumableItem));
                    return;
                }

                for (int i = 0; i < Math.Abs(val); i++) inv.Remove("Еда", InventoryItemType.ConsumableItem);
                return;
            }
            throw new InvalidOperationException("Не удалось выполнить AddFood()");
        }

        public static void PistolsToConsole(this Inventory inventory)
        {
            if (inventory is Inventory<InventoryItem> inv)
            {
                var pistols = inv.GetList("Пистолет", InventoryItemType.Weapon);
                foreach (var pistol in pistols)
                {
                    if (pistol is Pistol p)
                    {
                        var loadSign = p.Loaded ? "[*]" : "[ ]";
                        GameWorld.Instance.SendNotification(new NotificationEventArgs(string.Format("[{0, -10}{1, 4}]", pistol.Name, loadSign)));
                    }

                }
                return;
            }
            throw new InvalidOperationException("Не удалось выполнить PistolsToConsole()");
        }
    }
}
