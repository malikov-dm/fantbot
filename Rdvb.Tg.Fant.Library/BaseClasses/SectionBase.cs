﻿using Rdvb.Tg.Fant.Library.Controllers;
using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public abstract class SectionBase
    {
        #region Serialization
        public int Id { get; set; }
        public string SectionId { get; set; }
        public string Title { get; set; }
        public List<string> Body { get; set; } = new List<string>();
        public List<Enemy> Enemies { get; set; } = new List<Enemy>();
        public List<Link> Links { get; set; } = new List<Link>();
        public List<Section> Sections { get; set; } = new List<Section>();
        public Dictionary<string, string> SectionAttributes { get; set; } = new Dictionary<string, string>();
        //public int Str { get; set; }
        public bool ShouldSerializeEnemies()
        {
            return (Enemies?.Count > 0);
        }

        public bool ShouldSerializeLinks()
        {
            return (Links?.Count > 0);
        }

        public bool ShouldSerializeSections()
        {
            return (Sections?.Count > 0);
        }

        public bool ShouldSerializeSectionAttributes()
        {
            return (SectionAttributes?.Count > 0);
        }
        #endregion


        public SectionBase(string sectionId, string title) : this()
        {
            SectionId = sectionId;
            Title = title;

            if (int.TryParse(title, out int id))
            {
                Id = id;
            }
        }

        public SectionBase()
        {
            Entering += CurrentSection_SectionEntering;
            Entered += CurrentSection_SectionEntered;
            Leaving += CurrentSection_SectionLeaving;
            Leaved += CurrentSection_SectionLeaved;
            InventoryChanged += CurrentSection_InventoryChanged;
            ChoiceLinkAppeared += CurrentSection_ChoiceLinkAppeared;
        }

        //public abstract SectionBase MoveNext(SectionBase sectionBase);
        public abstract Section MoveNext(Section sectionBase, Action<Player> action = null);
        public abstract Section MoveNext(string linkClientId, Action<Player> action = null);
        public abstract Section MoveNext(int targetSectionId, Action<Player> action = null);
        public abstract void Render();

        protected internal event SectionEventHandler Entering;
        protected internal event SectionEventHandler Entered;
        protected internal event SectionEventHandler Leaving;
        protected internal event SectionEventHandler Leaved;
        protected internal event SectionEventHandler InventoryChanged;
        protected internal event SectionEventHandler ChoiceLinkAppeared;

        protected internal void CallEvent(SectionEventArgs e, SectionEventHandler handler)
        {
            if (handler != null && e != null)
                handler(this, e);
        }
        //protected internal void CallEvent(SectionEventArgs e, SectionEventHandler handler)
        //{
        //    if (handler != null && e != null)
        //        handler(this, e);
        //}

        protected virtual void OnSectionEntering(SectionEventArgs e)
        {
            CallEvent(e, Entering);
        }
        internal virtual void OnSectionLeaving(SectionEventArgs e)
        {
            CallEvent(e, Leaving);
        }
        internal virtual void OnSectionEntered(SectionEventArgs e)
        {
            CallEvent(e, Entered);
        }
        internal virtual void OnSectionLeaved(SectionEventArgs e)
        {
            CallEvent(e, Leaved);
        }
        internal virtual void OnInventoryChanged(SectionEventArgs e)
        {
            CallEvent(e, InventoryChanged);
        }

        internal virtual void OnChoiceLinkAppeared(SectionEventArgs e)
        {
            CallEvent(e, ChoiceLinkAppeared);
        }

        



        protected void CurrentSection_ChoiceLinkAppeared(object sender, SectionEventArgs e)
        {
            SectionStateController.OnChoiceLinkAppeared(sender, e);
        }

        protected void CurrentSection_SectionEntering(object sender, SectionEventArgs e)
        {
            SectionStateController.OnSectionEntering(sender, e);
        }

        protected void CurrentSection_SectionEntered(object sender, SectionEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
            SectionStateController.OnSectionEntered(sender, e);
        }

        protected void CurrentSection_SectionLeaving(object sender, SectionEventArgs e)
        {
            SectionStateController.OnSectionLeaving(sender, e);
        }

        protected void CurrentSection_SectionLeaved(object sender, SectionEventArgs e)
        {
            SectionStateController.OnSectionLeaved(sender, e);
        }

        protected void CurrentSection_InventoryChanged(object sender, SectionEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
            if (e.Section != null)
            {
                e.Player.Inventory.Add(Weapon.Create("Шпага отца", WeaponType.Rapier, new Buff(0, +1)));
            }
        }



        
    }
}
