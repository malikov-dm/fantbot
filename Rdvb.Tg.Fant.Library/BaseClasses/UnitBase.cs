﻿using Rdvb.Tg.Fant.Library.BaseClasses;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public abstract class UnitBase 
    {
        public string Name { get; set; }
        public int Str { get; set; }
        public int MinStr { get; set; }
        public int Dex { get; set; }
        public int MaxRoundCount { get; set; }

        
    }
}
