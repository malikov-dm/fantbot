﻿using Newtonsoft.Json;
using Rdvb.Tg.Fant.Library.BaseClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public abstract class GameWorldBase : BaseObject
    {
        public List<Section> Sections { get; set; } = new List<Section>();


        public bool ShouldSerializeSections()
        {
            return (Sections?.Count > 0);
        }

        
    }
}
