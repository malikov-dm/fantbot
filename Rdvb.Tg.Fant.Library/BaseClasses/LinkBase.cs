﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public abstract class LinkBase
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string TargetSectionId { get; set; }
        public bool IsHidden { get; set; }
        public Dictionary<string, string> LinkAttributes { get; set; }
        public bool ShouldSerializeLinkAttributes()
        {
            return (LinkAttributes?.Count > 0);
        }
    }
}
