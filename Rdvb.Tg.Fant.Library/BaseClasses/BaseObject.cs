﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.BaseClasses
{
    public abstract class BaseObject
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public BaseObject()
        {
            logger.Info("BaseObject ctor");
        }
        public event NotificationEventHandler NotificationEvent;

        protected internal void CallEvent(NotificationEventArgs e, NotificationEventHandler handler)
        {
            if (handler != null && e != null)
                handler(this, e);
        }

        public void SendNotification(NotificationEventArgs e)
        {
            CallEvent(e, NotificationEvent);
        }
    }
}
