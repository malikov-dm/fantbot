﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Enums
{
    public enum UnitStatus
    {
        Alive,
        Dead,
        Defeated,
        NoHonor
    }
}
