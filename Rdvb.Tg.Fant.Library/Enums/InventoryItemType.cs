﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Enums
{
    public enum InventoryItemType
    {
        Weapon,
        ConsumableItem
    }
}
