﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Enums
{
    public enum FightStatus
    {
        Win,
        Dead,
        Round,
        Defeated
    }
}
