﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Rdvb.Tg.Fant.Library.Enums;

namespace Rdvb.Tg.Fant.Library
{
    public class Link : LinkBase
    {

        public string ClientId { get; set; }

        public string Description { get; set; }
        public LinkType LinkType { get; set; } = LinkType.Simple;
        public LinkEvent LinkEvent { get; set; } = LinkEvent.Empty;

        public Link()
        {

        }
        public Link(string targetSectionId, string title, bool isHidden = false)
        {
            TargetSectionId = targetSectionId;
            Title = title;
            IsHidden = isHidden;
            LinkAttributes = new Dictionary<string, string>();
            if (int.TryParse(title, out int id))
            {
                Id = id;
            }


        }

        public override string ToString()
        {
            return $"[ {ClientId} > {TargetSectionId} ] {Description}";
        }
    }
}
