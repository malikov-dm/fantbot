﻿using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class ConsumableItem : InventoryItem
    {
        public ConsumableItemType ConsumableItemType { get; }
        protected ConsumableItem(string name, ConsumableItemType consumableItemType) : base(name, InventoryItemType.ConsumableItem) => (Name, ConsumableItemType) = (name, consumableItemType);

        public static ConsumableItem Create(string name, ConsumableItemType consumableItemType)
        {
            return new ConsumableItem(name, consumableItemType);
        }
    }
}
