﻿using Rdvb.Tg.Fant.Library.Entities.Sections;
using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class Section : SectionBase
    {
        private Player _player;

        public Player Player
        {
            get;
            set;
        }

        public SectionState SectionState { get; set; }

        public Section() : base() { }

        public Section(string sectionId, string title) : base(sectionId, title) { }

        //public override SectionBase MoveNext(SectionBase sectionBase)
        //{
        //    Player = sectionBase.Player;
        //    Player.Section = sectionBase;
        //    return sectionBase;
        //}

        public override Section MoveNext(Section section, Action<Player> action = null)
        {
            Player = section.Player;
            Player.Section = section;
            OnSectionEntering(new SectionEventArgs($"Вход на секцию.", _player: Player, _section: Player.Section));
            OnSectionEntered(new SectionEventArgs($"Вошел на секцию.", _player: Player, _section: Player.Section));
            action?.Invoke(Player);
            return section;
        }

        public override Section MoveNext(string linkClientId, Action<Player> action = null)
        {
            var link = GetLink(linkClientId);

            OnSectionLeaving(new SectionEventArgs($"Выход из секции.", Player, link, Player.Section));
            OnSectionLeaved(new SectionEventArgs($"Вышел из секции.", Player, link, Player.Section));

            var section = GameWorld.GetSectionById(link.Id);
            Player.CurrentLink = link;
            return MoveNext(section, action);
        }

        public override Section MoveNext(int targetSectionId, Action<Player> action = null)
        {
            var section = GameWorld.GetSectionById(targetSectionId);
            //Player.CurrentLink = null;
            //OnSectionLeaving(new SectionEventArgs($"Выход из секции.", _player: Player, _section: Player.Section));
            //OnSectionLeaved(new SectionEventArgs($"Вышел из секции.", _player: Player, _section: Player.Section));

            return MoveNext(section, action);
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }

        public Link GetLink(int targetSectionId)
        {
            var link = Links.FirstOrDefault(x => x.Id == targetSectionId);
            if (link == null)
            {
                throw new ArgumentException($"С секции {Id} невозможно перейти на секцию {targetSectionId}.");
            }
            return link;
        }

        public Link GetLink(string linkClientId)
        {
            var link = Links.FirstOrDefault(x => x.ClientId == linkClientId);
            if (link == null)
            {
                throw new ArgumentException($"На секции {Id} невозможно невозможно выбрать опцию {linkClientId}.");
            }
            return link;
        }

        #region properties
        public bool HasQuest
        {
            get => Quest != null;
        }

        #endregion

        public void Enter(Player player)
        {
            _player = player;
            OnSectionEntering(new SectionEventArgs($"Вход на секцию.", _player: player, _section: player.CurrentSection));
            OnSectionEntered(new SectionEventArgs($"Вошел на секцию.", _player: player, _section: player.CurrentSection));
            if (GameWorld == null)
            {
                GameWorld = player.GameSession.GameWorld;
            }
        }

        public SectionBase Leave(Player player, string linkClientId)
        {
            var link = player.CurrentSection.GetLink(linkClientId);
            var section = Leave(player, link);
            return section;
        }

        public SectionBase Leave(Player player, int targetSectionId)
        {
            var link = player.CurrentSection.GetLink(targetSectionId);
            var section = Leave(player, link);
            return section;
        }

        private SectionBase Leave(Player player, Link link)
        {
            player.LeaveSection(link);
            OnSectionLeaving(new SectionEventArgs($"Выход из секции.", _player: player, _link: link));
            OnSectionLeaved(new SectionEventArgs($"Вышел из секции.", _player: player, _link: link));
            return player.TargetSection;
        }



        internal List<Link> LinksBag { get; set; } = new List<Link>();
        public List<Link> GetVisibleLinks(Player player)
        {
            var res = Links;
            switch (SectionType)
            {
                case SectionType.CheckLuck:
                    {
                        var val = CurrentCheckLuckResult ? "1" : "0";
#if DEBUG

#else
                        res = Links.Where(x => x.LinkAttributes.Contains(new KeyValuePair<string, string>("luck", val))).ToList();
#endif
                        break;
                    }
                case SectionType.Fighting:
                    {
                        if (player.Status != UnitStatus.Defeated)
                        {
                            res = Links;
                        }
                        else
                        {
                            res = null;
                        }
                        break;
                    }
            }
            return res;
        }
        //public List<Section> Sections { get; set; }
        public Quest Quest { get; set; }

        public SectionType SectionType { get; set; }

        public GameWorld GameWorld { get; set; }

        internal bool CurrentCheckLuckResult;
        internal Dice Dice = new Dice();

        internal bool HasAliveEnemies
        {
            get
            {
                var cnt = 0;
                if (Enemies != null) cnt = Enemies.Where(x => x.Status == UnitStatus.Alive).Count();
                return cnt > 0;
            }
        }

        public override string ToString()
        {
            return $"[ {Title} ]";
        }

        
    }
}
