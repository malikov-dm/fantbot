﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using Rdvb.Tg.Fant.Library.Entities.Sections;

namespace Rdvb.Tg.Fant.Library
{
    public class GameWorld : GameWorldBase
    {
        public DateTime LastWriteTime { get; set; }
        public ObservableCollection<GameSession> GameSessions { get; set; } = new ObservableCollection<GameSession>();

        public Player Player { get; set; }

        public Section GetSectionById(int sectionId)
        {
            var section = Sections.FirstOrDefault(x => x.Id == sectionId);
            section.GameWorld = this;
            section.Player = Player;
            section.SectionState = new SimpleSectionState(section);
            if (section == null)
            {
                throw new ArgumentException($"Секции с Id: {sectionId} не существует.");
            }
            return section;

        }
        public GameWorld()
        {
            Instance = this;
        }
        public static GameWorld Instance;


    }
}
