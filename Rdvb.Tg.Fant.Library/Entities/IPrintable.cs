﻿namespace Rdvb.Tg.Fant.Library
{
    public interface IPrintable
    {
    }
    public interface IPrintable<T> : IPrintable
    {
    }
}