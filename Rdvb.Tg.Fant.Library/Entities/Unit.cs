﻿using Rdvb.Tg.Fant.Library.Entities;
using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class Unit: UnitBase, IPrintable
    {
        //protected int MaxStr { get; private set; }
        

        private int _honor;
        internal int _faith = 1;

        public UnitStatus Status = UnitStatus.Alive;

        public int Honor
        {
            get => _honor;
            set
            {
                if (value > 0)
                {
                    _honor = value;
                }
                else
                {
                    _honor = 0;
                    Status = UnitStatus.NoHonor;
                    OnNoHonor(new UnitEventArgs("Игрок обесчещен"));
                }
            }
        }
       
        public string Wounds { get; set; }
        public Stats Stats { get; private set; }
        //public GameWorld GameWorld { get; set; }

        protected internal event UnitStateHandler Dead;
        protected internal event UnitStateHandler Defeated;
        protected internal event UnitStateHandler NoHonor;
        protected internal event UnitStateHandler NoMoney;
        protected internal event UnitStateHandler SwimmingChecking;
        protected internal event UnitStateHandler TwoHandedChecking;
        protected internal event UnitEventHandler FaithChecking;

        //public event NotificationEventHandler NotificationEvent;

        //protected internal void CallEvent(NotificationEventArgs e, NotificationEventHandler handler)
        //{
        //    if (handler != null && e != null)
        //        handler(this, e);
        //}

        protected internal void CallEvent(UnitEventArgs e, UnitStateHandler handler)
        {
            if (handler != null && e != null)
                handler(this, e);
        }

        protected internal void CallEvent(UnitEventArgs e, UnitEventHandler handler)
        {
            if (handler != null && e != null)
                handler(this, e);
        }

        internal virtual void OnDead(UnitEventArgs e)
        {
            CallEvent(e, Dead);
        }
        internal virtual void OnDefeated(UnitEventArgs e)
        {
            CallEvent(e, Defeated);
        }
        internal virtual void OnNoHonor(UnitEventArgs e)
        {
            CallEvent(e, NoHonor);
        }
        internal virtual void OnNoMoney(UnitEventArgs e)
        {
            CallEvent(e, NoMoney);
        }
        internal virtual void OnTwoHandedChecking(UnitEventArgs e)
        {
            CallEvent(e, TwoHandedChecking);
        }
        internal virtual void OnSwimmingChecking(UnitEventArgs e)
        {
            CallEvent(e, SwimmingChecking);
        }
        internal virtual void OnFaithChecking(UnitEventArgs e)
        {
            CallEvent(e, FaithChecking);
        }

        //protected virtual void GameWorld.Instance.SendNotification(NotificationEventArgs e)
        //{
        //    CallEvent(e, NotificationEvent);
        //}

        public void Init()
        {
            Stats = new Stats(this);
            Stats.MaxStr = Stats.Str;
            Dead += Unit_Dead;
            NoHonor += Unit_NoHonor;
            NoMoney += Unit_NoMoney;
            SwimmingChecking += Unit_SwimmingChecking;
            TwoHandedChecking += Unit_TwoHandedChecking;
            Defeated += Unit_Defeated;
            //NotificationEvent += Unit_Notification;
            FaithChecking += Unit_FaithChecking;
        }

        protected internal virtual void Unit_FaithChecking(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs($"User: { e.Message}"));
        }

        

        private void Unit_Notification(object sender, NotificationEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Unit_Defeated(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }

        private void Unit_TwoHandedChecking(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }

        private void Unit_SwimmingChecking(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }

        private void Unit_NoMoney(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }

        private void Unit_NoHonor(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }

        protected internal virtual void Unit_Dead(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }

        public override string ToString() => string.Format("[{0, -20}{1, 5}][Dex: {2, 3}]", Name, $"{Stats.Str}/{Stats.MaxStr}", Dex);

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            Init();

        }
    }
}
