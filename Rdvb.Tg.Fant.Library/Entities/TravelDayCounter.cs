﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class TravelDayCounter
    {
        public TravelDayCounter(Player player)
        {
            _player = player;
        }

        private Player _player;

        public int TotalDay { get; private set; }

        internal void Add(int daysCount)
        {
            _player.Full = false;
            TotalDay += daysCount;
        }


    }
}
