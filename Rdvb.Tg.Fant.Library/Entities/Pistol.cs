﻿using System;
using System.Collections.Generic;
using System.Text;
using Rdvb.Tg.Fant.Library.Enums;

namespace Rdvb.Tg.Fant.Library
{
    public class Pistol : Weapon
    {
        public bool Loaded { get; private set; } = true;
        protected Pistol(string name, Buff buff = null) : base(name, WeaponType.Pistol, buff) { }

        public static Pistol Create(string name, Buff buff = null)
        {
            return new Pistol(name, buff);
        }

        public Pistol Fire(bool atrand = false)
        {
            OnInventoryChanged(new InventoryItemEventArgs("Выстрел из пистолета"));
            Loaded = false;
            return this;
        }

        public Pistol Reload()
        {
            OnInventoryChanged(new InventoryItemEventArgs("Перезарядка пистолета"));
            if (Inventory is Inventory<InventoryItem> i)
            {
                if (i.ContainsAmmo)
                {
                    i.Remove("Пуля", InventoryItemType.ConsumableItem);
                    Loaded = true;
                    return this;
                }
            }
            throw new Exception("Патроны кончились");
        }
        //protected override void InventoryItem_InventoryChanged(object sender, InventoryItemEventArgs e)
        //{
        //    base.InventoryItem_InventoryChanged(sender, e);
        //}
    }
}
