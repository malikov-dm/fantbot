﻿using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class Fighting
    {
        private int _maxRoundCount { get; set; } = 100;
        private int _currentRoundCount { get; set; } = 0;
        private Dice _dice = new Dice();


        public Fighting() { }

        public Fighting(int maxRoundCount) : this()
        {
            _maxRoundCount = maxRoundCount;
        }

        /// <summary>
        /// Битва с несколькими врагами
        /// </summary>
        /// <param name="player">Игрок</param>
        /// <param name="enemies">Враги</param>
        /// <param name="inorder">Враги нападают по очереди</param>
        /// <returns></returns>
        public FightStatus Start(Player player, List<Enemy> enemies, bool inorder = false)
        {
            var res = FightStatus.Win;
            if (inorder)
            {
                foreach (Enemy enemy in enemies)
                {
                    if (enemy.MaxRoundCount > 0)
                    {
                        _maxRoundCount = enemy.MaxRoundCount;
                    }
                    var status = Start(player, enemy);
                    if (status != FightStatus.Win)
                    {
                        res = status;
                        break;
                    }
                }

                return res;
            }

            res = Start(player, enemies);

            return res;
        }

        private FightStatus Start(Player player, Enemy enemy)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs("Start"));
            player.ToConsole();
            enemy.ToConsole();

            if (enemy.MaxRoundCount > 0)
            {
                _maxRoundCount = enemy.MaxRoundCount;
            }

            player.CurrentEnemy = enemy;
            if (player.CanUseSecretHit)
            {
                enemy.Str -= 4;
            }
            var status = FightStatus.Win;
            bool alive = true;
            while (alive)
            {



                switch (player.Status)
                {
                    case UnitStatus.Dead:
                        status = FightStatus.Dead;
                        return status;
                    case UnitStatus.Defeated:
                        status = FightStatus.Defeated;
                        return FightStatus.Defeated;
                }

                switch (enemy.Status)
                {
                    case UnitStatus.Dead:
                    case UnitStatus.Defeated:
                        status = FightStatus.Win;
                        return FightStatus.Defeated;
                }
                GameWorld.Instance.SendNotification(new NotificationEventArgs($"==================== Round: {_currentRoundCount} ===================="));
                _currentRoundCount++;
                var pdice = _dice.Throw6();
                var ppow = pdice + player.Dex;
                var epow = _dice.Throw6() * 2 + enemy.Dex;
                if (ppow == epow)
                {
                    enemy.Str -= player.CanUseDagger(pdice) ? 2 : 0;
                }
                else if (ppow > epow)
                {
                    enemy.Str -= player.CanUseDagger(pdice) ? 3 : 2;
                }
                else
                {
                    enemy.Str -= player.CanUseDagger(pdice) ? 2 : 0;
                    player.Stats.Str -= 2;
                }
            }

            player.ToConsole();
            enemy.ToConsole();

            // но если счетчик раундов превысил максимальное число - выходим с со статусом Round
            if (_currentRoundCount > _maxRoundCount)
            {
                status = FightStatus.Round;
            }

            return status;
        }


        private FightStatus Start(Player player, List<Enemy> enemies)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs("Start"));
            var status = FightStatus.Win;
            bool alive = true;

            while (alive)
            {
                _currentRoundCount++;
                GameWorld.Instance.SendNotification(new NotificationEventArgs($"==================== Round: {_currentRoundCount} ===================="));
                var es = enemies.Where(x => x.Status == UnitStatus.Alive).ToList();
                if (es.Count == 0)
                {
                    break;
                }
                status = FightOnce(player, es[0]);
                for (int i = 1; i < es.Count; i++)
                {
                    status = BiteOnce(player, es[i]);
                }

                if (status != FightStatus.Win)
                {
                    alive = false;
                }


            }

            if (_currentRoundCount > _maxRoundCount)
            {
                status = FightStatus.Round;
            }

            return status;
        }

        private FightStatus FightOnce(Player player, Enemy enemy)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs("FightOnce"));

            if (enemy.MaxRoundCount > 0)
            {
                _maxRoundCount = enemy.MaxRoundCount;
            }

            player.CurrentEnemy = enemy;
            if (player.CanUseSecretHit)
            {
                enemy.Stats.Str -= 4;
            }

            var status = FightStatus.Win;

            switch (player.Status)
            {
                case UnitStatus.Dead:
                    status = FightStatus.Dead;
                    return status;
                case UnitStatus.Defeated:
                    status = FightStatus.Defeated;
                    return FightStatus.Defeated;
            }

            switch (enemy.Status)
            {
                case UnitStatus.Dead:
                case UnitStatus.Defeated:
                    status = FightStatus.Win;
                    return FightStatus.Defeated;
            }


            var pdice = _dice.Throw6();
            var ppow = pdice * 2 + player.Dex;
            var epow = _dice.Throw6() * 2 + enemy.Dex;

            if (ppow == epow)
            {
                enemy.Stats.Str -= player.CanUseDagger(pdice) ? 2 : 0;
            }
            else if (ppow > epow)
            {
                enemy.Stats.Str -= player.CanUseDagger(pdice) ? 3 : 2;
            }
            else
            {
                enemy.Stats.Str -= player.CanUseDagger(pdice) ? 2 : 0;
                player.Stats.Str -= 2;
            }

            player.ToConsole();
            enemy.ToConsole();
            return status;
        }

        private FightStatus BiteOnce(Player player, Enemy enemy)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs("BiteOnce"));
            var status = FightStatus.Win;

            switch (player.Status)
            {
                case UnitStatus.Dead:
                    status = FightStatus.Dead;
                    return status;
                case UnitStatus.Defeated:
                    status = FightStatus.Defeated;
                    return FightStatus.Defeated;
            }

            var ppow = _dice.Throw6() * 2 + player.Dex;
            var epow = _dice.Throw6() * 2 + enemy.Dex;
            if (epow > ppow)
            {
                player.Stats.Str -= 2;
            }

            player.ToConsole();
            enemy.ToConsole();
            return status;
        }


    }
}
