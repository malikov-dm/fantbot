﻿using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class Weapon: InventoryItem
    {
        public WeaponType WeaponType { get; }
        public Buff Buff { get; }
        protected Weapon(string name, WeaponType weaponType, Buff buff = null) : base(name, InventoryItemType.Weapon) => (Name, WeaponType, Buff) = (name, weaponType, buff);

        public static Weapon Create(string name, WeaponType weaponType, Buff buff = null)
        {
            return new Weapon(name, weaponType, buff);
        }
    }
}
