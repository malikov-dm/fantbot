﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Globalization;
using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Factories;
using Rdvb.Tg.Fant.Library.Shared;
using Rdvb.Tg.Fant.Library.Entities;

namespace Rdvb.Tg.Fant.Library
{
    public class Player : Unit
    {
        public Section Section { get; set; }
        public GameSession GameSession { get; set; }
        public Section CurrentSection { get; set; }
        public Section TargetSection { get; set; }
        public Link CurrentLink { get; set; } = new Link();
        public Enemy CurrentEnemy { get; set; }

        public Inventory<InventoryItem> Inventory { get; set; }
        public List<Quest> Quests { get; set; } = new List<Quest>();
        public SpecialAbility SpecialAbility { get; set; }


        public bool HasSwimmingAbility
        {
            get
            {
                OnSwimmingChecking(new UnitEventArgs("Происходит проверка на способность плавать"));
                return SpecialAbility == SpecialAbility.Swimming;
            }
        }
        public bool HasTwoHandedAbility
        {
            get
            {
                OnTwoHandedChecking(new UnitEventArgs("Происходит проверка на способность стрелять из двух пистолетов"));
                return SpecialAbility == SpecialAbility.TwoHanded;
            }
        }
        

        public bool CanUseDagger(int dice)
        {

            var res = SpecialAbility == SpecialAbility.Dagger && Inventory.ContainsDagger && dice % 2 == 0;
            if (res)
            {
                GameWorld.Instance.SendNotification(new NotificationEventArgs($"{Name} использует удар кинжалом"));
            };
            return res;

        }
        public bool CanUseSecretHit
        {
            get
            {
                var res = SpecialAbility == SpecialAbility.SecretHit && Inventory.ContainsRapier && CurrentEnemy.SecretHitAvailable;
                if (res)
                {
                    CurrentEnemy.SecretHitAvailable = false;
                    GameWorld.Instance.SendNotification(new NotificationEventArgs($"{Name} использует секретный удар шпагой"));
                };
                return res;
            }
        }
        public TravelDayCounter TravelDayCounter { get; }
        public bool Full { get; internal set; }


        public bool Faith
        {
            get
            {
                return _faith > 0;
            }
        }
        public bool HasFaith
        {
            get
            {
                OnFaithChecking(new UnitEventArgs($"Проверка на наличие веры"));
                return _faith > 0;
            }
        }
        public void AddFaith(int val)
        {
            if (val != 0)
            {
                _faith += val;
                _faith = _faith < 0 ? 0 : _faith;
            }
        }

        internal bool EatUp()
        {
            if (!Full)
            {
                Full = !Full;
                return true;
            }

            return false;
        }


        public Player(string name) : this(name, 20, 30)
        {
            
        }

        public Player(string name, int str, int dex, SpecialAbility specialAbility = SpecialAbility.Dagger)
        {
            Init();
            Name = name;
            Stats.MaxStr = str;
            Stats.Str = Stats.MaxStr;

            Dex = dex;
            Honor = 3;
            SpecialAbility = specialAbility;
            TravelDayCounter = new TravelDayCounter(this);
            Inventory = new Inventory<InventoryItem>(this, new InventoryItemFactory());
            Inventory.Init();
            Inventory.AddMoney(10);
            GameWorld.Instance.SendNotification(new NotificationEventArgs("Пользователь создан"));
        }



        public List<string> StatsStrings
        {
            get
            {
                return GetStats();
            }
        }
        private List<string> GetStats()
        {
            var res = new List<String>
            {
                string.Format("[ STR: {0, 8} | DEX: {1, 8} | M: {2, 8} | D: {3, 6} | Food: {4, 5} | Full: {5, 5} ]"
                , $"{Stats.Str}/{Stats.MaxStr}", Dex, Inventory.Money, TravelDayCounter.TotalDay, Inventory.Food, Full),
                string.Format("[ Honor: {0, 6} | Faith: {1, 6} | Wounds: {2, 3} | SA: {3, 5} ]", Honor, Faith, Wounds, SpecialAbility)
            };
            return res;
        }

        public string QuestsString
        {
            get
            {
                string res = "Квесты: ";
                foreach (var q in Quests)
                {
                    res += string.Format("[{0, -10}{1, 4}]    ", q.Name, "");
                }
                return res;
            }
        }

        [Obsolete]
        public Section EnterSection(int sectionId)
        {
            var section = GameSession.GameWorld.GetSectionById(sectionId);
            EnterSection(section);
            return section;
        }
        [Obsolete]
        public Section EnterSection(Section section)
        {
            CurrentSection = section;
            CurrentSection.Enter(this);
            return section;
        }
        [Obsolete]
        public SectionBase LeaveSection(string linkClientId)
        {
            return CurrentSection.Leave(this, linkClientId);
        }
        [Obsolete]
        public SectionBase LeaveSection(int targetSectionId)
        {
            return CurrentSection.Leave(this, targetSectionId);
        }
        [Obsolete]
        public void LeaveSection(Link link)
        {
            CurrentLink = link;
            var ncs = GameSession.GameWorld.GetSectionById(link.Id);
            TargetSection = ncs;
            //return ncs;
        }

        public void BeginQuest(Quest quest)
        {
            if (quest == null) return;

            if (!Quests.Any(x => x.Name == quest.Name))
            {
                Quests.Add(quest);
            }
        }


        protected internal override void Unit_FaithChecking(object sender, UnitEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs($"Player: { e.Message}"));
        }

        protected internal override void Unit_Dead(object sender, UnitEventArgs e)
        {
            if(sender is Player player)
            {
                GameWorld.Instance.SendNotification(new NotificationEventArgs("Можно перенаправить игрока на секцию GAME OVER по STR."));
                player.Section.MoveNext(GenGameSections.GameOverByStrSection());
            }
        }
    }
}
