﻿using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Factories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class Inventory
    {

    }
    public class Inventory<T> : Inventory, IPrintable<T>
        where T : InventoryItem
    {
        private decimal _money;
        private ObservableCollection<T> _inventory = new ObservableCollection<T>();
        private InventoryItemAbstractFactory<T> _factory;
        private List<KeyValuePair<T, int>> _groupedInventory = new List<KeyValuePair<T, int>>();

        public Player Player { get; }
        public bool ContainsDagger
        {
            get
            {
                return _inventory.Where(x => x.InventoryItemType == InventoryItemType.Weapon && (x as Weapon).WeaponType == WeaponType.Dagger).Count() > 0;
            }
        }
        public bool ContainsRapier
        {
            get
            {
                return _inventory.Where(x => x.InventoryItemType == InventoryItemType.Weapon && (x as Weapon).WeaponType == WeaponType.Rapier).Count() > 0;
            }
        }
        public bool ContainsAmmo
        {
            get
            {
                return _inventory.Where(x => x.InventoryItemType == InventoryItemType.ConsumableItem && (x as ConsumableItem).ConsumableItemType == ConsumableItemType.Ammo).Count() > 0;
            }
        }
        public bool ContainsFood
        {
            get
            {
                return _inventory.Where(x => x.InventoryItemType == InventoryItemType.ConsumableItem && (x as ConsumableItem).ConsumableItemType == ConsumableItemType.Food).Count() > 0;
            }
        }
        public bool ContainsPistol
        {
            get
            {
                return _inventory.Where(x => x.InventoryItemType == InventoryItemType.Weapon && (x as Weapon).WeaponType == WeaponType.Pistol).Count() > 0;
            }
        }
        public bool ContainsLoadedPistol
        {
            get
            {
                return _inventory.Where(
                    x => x.InventoryItemType == InventoryItemType.Weapon
                        && (x as Weapon).WeaponType == WeaponType.Pistol
                        && (x as Pistol).Loaded
                    ).Count() > 0;
            }
        }
        public void ReloadPistols()
        {
            var coll = _inventory.Where(
                            x => x.InventoryItemType == InventoryItemType.Weapon
                            && (x as Weapon).WeaponType == WeaponType.Pistol
                            && !(x as Pistol).Loaded
                    ).ToList();

            foreach (var p in coll)
            {
                if (p is Pistol pistol)
                {
                    pistol.Reload();
                }
            }
        }
        public Pistol GetLoadedPistol()
        {
            var p = _inventory.FirstOrDefault(
                            x => x.InventoryItemType == InventoryItemType.Weapon
                           && (x as Weapon).WeaponType == WeaponType.Pistol
                           && (x as Pistol).Loaded);

            return (p is Pistol pistol) ? pistol : null;
        }
        public decimal Money { get => _money; }
        public int Food
        {
            get
            {
                return _inventory.Where(
                            x => x.InventoryItemType == InventoryItemType.ConsumableItem
                            && string.Equals(x.Name, "Еда", StringComparison.InvariantCultureIgnoreCase)).Count();
            }
        }


        public Inventory(Player player, InventoryItemAbstractFactory<T> factory) : this(player)
        {
            _factory = factory;
        }

        private Inventory(Player player)
        {
            Player = player;
            _inventory.CollectionChanged += Inventory_CollectionChanged;
        }

        private void Inventory_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs("Инвентарь изменен"));
        }

        public void Add(T item)
        {
            item.Inventory = this;
            _inventory.Add(item);
        }

        public void Add(T item, int count)
        {
            item.Inventory = this;
            for (int i = 0; i < count; i++)
                _inventory.Add(item);
        }

        public void Init()
        {
            if(_factory == null)
            {
                throw new Exception("_factory is null");
            }
            Add(_factory.CreateWeapon("Шпага", WeaponType.Rapier));
            Add(_factory.CreateWeapon("Кинжал", WeaponType.Dagger));
            Add(_factory.CreatePistol("Пистолет"));
            Add(_factory.CreatePistol("Пистолет"));
            Add(_factory.CreateConsumableItem("Пуля", ConsumableItemType.Ammo), 5);
            Add(_factory.CreateConsumableItem("Еда", ConsumableItemType.Food), 2);
        }
        public List<T> GetList(string itemName, InventoryItemType inventoryItemType)
        {
            var res = _inventory.Where(x => string.Equals(x.Name, itemName, StringComparison.InvariantCultureIgnoreCase) && x.InventoryItemType == inventoryItemType).ToList();
            return res;
        }
        public void Remove(string itemName, InventoryItemType inventoryItemType)
        {
            var item = _inventory.FirstOrDefault(x => string.Equals(x.Name, itemName, StringComparison.InvariantCultureIgnoreCase) && x.InventoryItemType == inventoryItemType);
            _inventory.Remove(item);
        }

        public bool AddMoney(decimal value)
        {
            if (_money + value >= 0)
            {
                _money += value;
                return true;
            }

            return false;
        }


        /// <summary>
        ///  Если денег больше, чем требуют, отдаем сколько нужно. Если нет - сколько есть.
        /// </summary>
        /// <param name="sum"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        internal decimal TryGetMoneySum(decimal sum)
        {
            if (sum <= 0)
            {
                return Money - Math.Abs(sum) >= 0 ? sum : -Money;
            }

            return Money + sum;
        }

        private IEnumerable<Tuple<string, int>> GetGroupedInventory()
        {
            var res = _inventory
                  .GroupBy(x => x.Name)
                  .Select(n => Tuple.Create(n.Key, n.Count()));
            return res;
        }
        public override string ToString()
        {
            string res = "Инвент: ";
            foreach (var str in GetGroupedInventory())
            {
                res += string.Format("[{0, -10}{1, 4}]    ", str.Item1, str.Item2);
            }
            return res;
        }


    }
}
