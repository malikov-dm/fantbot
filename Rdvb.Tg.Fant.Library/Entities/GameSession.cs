﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Rdvb.Tg.Fant.Library
{
    public class GameSession
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public readonly Guid SessionId;
        public readonly Player Player;
        public List<int> UserPath;

        [Obsolete]
        public GameWorld GameWorld { get; set; }

        [Obsolete]
        public GameSession(GameWorld gameWorld, Player player)
        {
            SessionId = Guid.NewGuid();
            GameWorld = gameWorld;
            Player = player;
            Player.GameSession = this;
            UserPath = new List<int>();
        }

        public GameSession(Player player)
        {
            
            SessionId = Guid.NewGuid();
            Player = player;
            Player.GameSession = this;
            UserPath = new List<int>();
            logger.Info($"GameSession ctor :: {SessionId} :: {player.Name}");
        }

    }
}
