﻿using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class InventoryItem
    {
        protected internal event InventoryItemEventHandler InventoryChanged;

        public Inventory Inventory { get; set; }
        public string Name { get; internal set; }
        public InventoryItemType InventoryItemType { get; internal set; }

        public static InventoryItem Create(string name, InventoryItemType inventoryItemType)
        {
            return new InventoryItem(name, inventoryItemType);
        }

        protected InventoryItem()
        {
            InventoryChanged += InventoryItem_InventoryChanged;
        }

        protected InventoryItem(string name, InventoryItemType inventoryItemType) : this() => (Name, InventoryItemType) = (name, inventoryItemType);

        protected internal void CallEvent(InventoryItemEventArgs e, InventoryItemEventHandler handler)
        {
            if (handler != null && e != null)
                handler(this, e);
        }

        protected virtual void OnInventoryChanged(InventoryItemEventArgs e)
        {
            CallEvent(e, InventoryChanged);
        }

        protected virtual void InventoryItem_InventoryChanged(object sender, InventoryItemEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));
        }
    }
}
