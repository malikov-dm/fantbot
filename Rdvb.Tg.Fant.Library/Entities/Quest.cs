﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class Quest
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Quest(string name, string desc = "") => (Name, Description) = (name, desc);
    }
}
