﻿using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Entities
{
    public class Stats
    {
        private Unit _unit;

        public int MaxStr { get; set; } = 0;
        public int MinStr { get; set; } = 0;

        public Stats(Unit unit) => _unit = unit;

        public int Str {
            get => _unit.Str;
            set
            {
                if (MaxStr == 0)
                {
                    MaxStr = _unit.Str;
                }
                if (value > 0 && value > MinStr)
                {
                    _unit.Str = value > MaxStr ? MaxStr : value;

                }
                else if (value > 0 && value <= MinStr)
                {
                    _unit.Str = value;
                    _unit.Status = UnitStatus.Defeated;
                    _unit.OnDefeated(new UnitEventArgs($"{_unit.Name} повержен"));
                }
                else
                {
                    _unit.Str = 0;
                    _unit.Status = UnitStatus.Dead;
                    _unit.OnDead(new UnitEventArgs($"{_unit.Name} убит"));
                }
            }
        }

    }
}
