﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class Buff
    {
        public int Str { get; }
        public int Dex { get; }

        public Buff(int str, int dex) => (Str, Dex) = (str, dex);
    }
}
