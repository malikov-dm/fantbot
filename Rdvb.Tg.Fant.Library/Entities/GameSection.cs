﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{
    public class GameSection : Section
    {
        public GameSection(SectionEventArgs e = null) : base()
        {
            if (e != null)
            {
                Links = new List<Link>
                            {
                                new Link()
                                {
                                    Id = e.Player.CurrentSection.Id,
                                    Title = e.Player.CurrentSection.Id.ToString(),
                                    TargetSectionId = $"n_{e.Player.CurrentSection.Id.ToString()}",
                                    Description = "Вернуться на предыдущий экран"
                                }
                                //,
                                //new Link()
                                //{
                                //    Id = 1,
                                //    Title = "1",
                                //    TargetSectionId = $"n_1",
                                //    Description = "Вернуться в начало"
                                //},
                            };
            }


        }

        //public override List<Link> Links
        //{
        //    get; set;
        //}
    }
}
