﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Entities.Sections
{
    public abstract class SectionState
    {
        public Section Section { get; set; }
        public abstract void ToSimpleState();
        public abstract void ToFightState();
    }
}
