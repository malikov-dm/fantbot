﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Entities.Sections
{
    public class FightSectionState : SectionState
    {
        public FightSectionState(SectionState sectionState)
        {
            Section = sectionState.Section;
        }

        public FightSectionState(Section section)
        {
            Section = section;
        }
        public override void ToFightState()
        {
            throw new NotImplementedException();
        }

        public override void ToSimpleState()
        {
            Section.SectionState = new SimpleSectionState(this);
        }
    }
}
