﻿using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Entities.Sections
{
    public class SimpleSectionState : SectionState
    {
        public SimpleSectionState(SectionState sectionState)
        {
            Section = sectionState.Section;
        }

        public SimpleSectionState(Section section)
        {
            Section = section;
        }


        public override void ToFightState()
        {
            Section.SectionState = new FightSectionState(this);
            // генерируем ссылки на пистолет, если они у нас есть
            if(Section.Player.CurrentLink.LinkEvent != LinkEvent.DoNothing)
            {
                Section.Player.CurrentEnemy = Section.Enemies.FirstOrDefault(x => x.Status == UnitStatus.Alive);
                Section.OnChoiceLinkAppeared(new SectionEventArgs($"Проверка возможности воспользоваться пистолетами", Section.Player));
            }
        }

        public override void ToSimpleState()
        {
            throw new NotImplementedException();
        }
    }
}
