﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Interfaces
{
    public interface ISection
    {
        int? Id { get; set; }
        string SectionId { get; set; }
        string SectionClientId { get; set; }
        string Title { get; set; }
    }
}
