﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Interfaces
{
    public interface IUnit
    {
        string Name { get; set; }
        int Str { get; set; }
        int Dex { get; set; }
    }
}
