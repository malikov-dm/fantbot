﻿using System;

namespace Rdvb.Tg.Fant.Library
{
    public class Book
    {
        public string Rules { get; set; }
        public string Intro { get; set; }
    }
}
