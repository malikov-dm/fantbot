﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using Rdvb.Tg.Fant.Library.Extensions;
using Rdvb.Tg.Fant.Library.Factories;
using Rdvb.Tg.Fant.Library.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Controllers
{
    public class SectionStateController
    {
        private static Dice _dice { get => DiceFactory.GetDice(); }
        public static void OnSectionEntering(object sender, SectionEventArgs e)
        {
            


            // чистим сгенерированные ссылки
            if (e.Section.Links != null)
            {
                e.Section.Links.RemoveAll(x => x.LinkType == LinkType.GameLink);
            }

            if (e.Section != null && e.Player.CurrentLink.LinkType == LinkType.GameLink)
            {
                e.Section.Links = e.Section.LinksBag;
            }

            // исполняем если это не генерируемая ссылка
            if (e.Section != null && e.Section.SectionType != SectionType.GameOver && e.Player.CurrentLink.LinkType != LinkType.GameLink)
            {
                e.Player.Inventory.AddMoney(e.Section.SectionAttributes.Parse<decimal>("money"));
                e.Player.Stats.Str += e.Section.SectionAttributes.Parse<int>("str");
                e.Player.TravelDayCounter.Add(e.Section.SectionAttributes.Parse<int>("days"));
                e.Player.Honor += e.Section.SectionAttributes.Parse<int>("honor");
                e.Player.Inventory.AddMoney(e.Player.Inventory.TryGetMoneySum(e.Section.SectionAttributes.Parse<decimal>("ifmoney")));
                if (e.Section.SectionAttributes.Parse<bool>("invchanged"))
                {
                    e.Section.OnInventoryChanged(new SectionEventArgs($"Событие invchanged", _player: e.Player, _section: e.Player.CurrentSection));
                }

                if (e.Section.SectionAttributes.TryParse<int>("chluck"))
                {
                    e.Section.SectionType = SectionType.CheckLuck;
                    e.Section.CurrentCheckLuckResult = _dice.CheckLuck();
                }

                if (e.Section.SectionAttributes.TryParse<int>("unloadpistol"))
                {
                    e.Player.Inventory.GetLoadedPistol().Fire();
                }
                if (e.Section.SectionAttributes.TryParse<int>("reloadpistol"))
                {
                    e.Player.Inventory.ReloadPistols();
                }

                e.Player.AddFaith(e.Section.SectionAttributes.Parse<int>("faith"));

            };

            if (e.Section != null)
            {
                if (e.Section.HasQuest)
                {
                    e.Player.BeginQuest(e.Section.Quest);
                }

                if (e.Player.CurrentLink.LinkEvent == LinkEvent.EatUp)
                {
                    e.Player.EatUp();
                }

                if (e.Player.CurrentLink.LinkEvent == LinkEvent.Shoot)
                {
                    e.Player.Inventory.GetLoadedPistol().Fire();
                    e.Player.CurrentEnemy.Stats.Str -= 4;
                }

                if (e.Player.CurrentLink.LinkEvent == LinkEvent.RandomShot)
                {
                    e.Player.Inventory.GetLoadedPistol().Fire();
                    if (_dice.CheckLuck()) { e.Player.CurrentEnemy.Stats.Str = 0; };
                }

                if (e.Section.SectionAttributes.TryParse<decimal>("foodcost"))
                {

                    e.Section.OnChoiceLinkAppeared(new SectionEventArgs($"На секции можно поесть за деньги", e.Player));
                }

                if (e.Section.SectionAttributes.TryParse<decimal>("ifnofood"))
                {

                    e.Section.OnChoiceLinkAppeared(new SectionEventArgs($"На секции можно съесть еду из инвентаря", e.Player));
                }

                //if (e.Section.HasAliveEnemies && e.Player.CurrentLink.LinkEvent != LinkEvent.DoNothing)
                //{
                //    e.Player.CurrentEnemy = e.Section.Enemies.FirstOrDefault(x => x.Status == UnitStatus.Alive);
                //    e.Section.OnChoiceLinkAppeared(new SectionEventArgs($"Проверка возможности воспользоваться пистолетами", e.Player));
                //}

                if (e.Section != null)
                {
                    var section = e.Section;
                    if (section.HasAliveEnemies)
                    {
                        section.SectionState.ToFightState();
                    }
                }

            }
        }

        public static void OnSectionEntered(object sender, SectionEventArgs e)
        {

            if (e.Section != null && e.Section.SectionType != SectionType.GameOver)
            {



                // Если на секции враги
                if (e.Section.HasAliveEnemies
                        && (!e.Player.Inventory.ContainsLoadedPistol
                                || (e.Player.CurrentLink.LinkType == LinkType.GameLink && e.Player.CurrentLink.LinkEvent == LinkEvent.DoNothing)))
                {
                    e.Section.SectionType = SectionType.Fighting;
                    var f = new Fighting();
                    var status = f.Start(e.Player, e.Section.Enemies);
                    if (status == FightStatus.Round)
                    {
                        // FightStatus.Round означает, что бой продлился больше указанного числа раундов, ссылку с атрибутом round надо убирать
                        e.Section.Links.Remove(e.Section.Links.FirstOrDefault(x => x.LinkAttributes.TryParse<int>("round")));

                    }

                    //оставить только ссылку, где есть атрибут round
                    var link = e.Section.Links.FirstOrDefault(x => x.LinkAttributes.TryParse<int>("round"));
                    if (link != null)
                    {
                        e.Section.Links = new List<Link> { link };
                    }

                }

            }
        }

        public static void OnChoiceLinkAppeared(object sender, SectionEventArgs e)
        {
            GameWorld.Instance.SendNotification(new NotificationEventArgs(e.Message));

            var section = sender as Section;
            section.LinksBag = new List<Link>(section.Links);
            // чистим сгенерированные ссылки
            section.Links.RemoveAll(x => x.LinkType == LinkType.GameLink);

            if (section.SectionAttributes.TryParse("foodcost", out decimal money)
                && section.SectionAttributes.TryParse("foodstr", out int str))
            {
                section.Links.AddSafely(GenGameLinks.GetFoodForMoneyLink(e.Player, section.Id, money, str));
            }

            if (section.SectionAttributes.TryParse("ifnofood", out int res))
            {
                section.Links.AddSafely(GenGameLinks.GetInventoryFoodLink(e.Player, section.Id, res));
            }

            if (section.HasAliveEnemies && e.Player.Inventory.ContainsLoadedPistol)
            {

                section.Links.Clear();
                // чтобы выстрелить из пистолета
                section.Links.AddSafely(GenGameLinks.GetPistolLink(e.Player, section.Id));
                section.Links.AddSafely(GenGameLinks.GetRandomPistolLink(e.Player, section.Id));
                section.Links.AddSafely(GenGameLinks.GetDoNothingLink(e.Player, section.Id));
            }
        }

        public static void OnSectionLeaving(object sender, SectionEventArgs e)
        {
            if (e.Link != null)
            {
                e.Player.Inventory.AddMoney(e.Link.LinkAttributes.Parse<decimal>("money"));
                e.Player.Stats.Str += e.Link.LinkAttributes.Parse<int>("str");
                e.Player.Inventory.AddFood(e.Link.LinkAttributes.Parse<int>("food"));
                e.Player.Honor += e.Link.LinkAttributes.Parse<int>("honor");
                if (e.Link.LinkAttributes.TryParse<int>("strdice"))
                {
                    e.Player.Stats.Str += _dice.Throw6() * e.Link.LinkAttributes.Parse<int>("strdice");
                };

                if (e.Link.LinkAttributes.TryParse<int>("swimming"))
                {
                    if (!e.Player.HasSwimmingAbility)
                    {
                        var sect = GenGameSections.NoSwimmingAbilitySection(e);
                        e.Player.TargetSection = sect;
                    }
                }

                if (e.Link.LinkAttributes.TryParse<int>("checkdual"))
                {
                    if (!e.Player.HasSwimmingAbility)
                    {
                        var sect = GenGameSections.NoTwoHandedAbilitySection(e);
                        e.Player.TargetSection = sect;
                    }
                }

                if (e.Link.LinkAttributes.TryParse<int>("faith"))
                {
                    if (!e.Player.HasFaith)
                    {
                        var sect = GenGameSections.NoMoreFaithSection(e);
                        e.Player.TargetSection = sect;
                    }
                    e.Player.AddFaith(e.Link.LinkAttributes.Parse<int>("faith"));
                }

                if (e.Link.LinkAttributes.TryParse<int>("reloadpistol"))
                {
                    e.Player.Inventory.ReloadPistols();
                }
            };
        }

        public static void OnSectionLeaved(object sender, SectionEventArgs e, [CallerMemberName] string callerMemberName = "")
        {
            if (!(sender is Section section))
            {
                GameWorld.Instance.SendNotification(new NotificationEventArgs($"Не могу определить секцию: [{callerMemberName}]"));
            }

            section = sender as Section;
            GameWorld.Instance.SendNotification(new NotificationEventArgs($"OnSectionLeaved: {section.SectionId}"));
        }
    }
}
