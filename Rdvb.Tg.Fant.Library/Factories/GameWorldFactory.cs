﻿using Rdvb.Tg.Fant.Library.Analyzer;
using Rdvb.Tg.Fant.Library;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Runtime.CompilerServices;
using Rdvb.Tg.Fant.Library.Builders;

namespace Rdvb.Tg.Fant.Library.Factories
{
    public static class GameWorldFactory
    {
        public static GameWorld CreateGameWorld(string filePath = @"input\book.json")
        {
            //var gwb = new GameWorldBuilder();
            var gw = GameWorldBuilder
                .LoadJson(filePath)
                .LoadSessions();

            gw.GameSessions.CollectionChanged += GameSessionsCollectionChanged;
            return gw;
        }

        public static GameWorld CreateGameWorldForTests(string folder, [CallerMemberName] string callerMemberName = "")
        {
            var gw = GameWorldBuilder
                .LoadJson($"{folder}\\{callerMemberName}.json")
                .LoadSessions();

            gw.GameSessions.CollectionChanged += GameSessionsCollectionChanged;
            return gw;
        }

        static void GameSessionsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add: // если добавление
                    {
                        GameWorld.Instance.SendNotification(new NotificationEventArgs($"Создана игровая сессия"));
                        break;
                    }
                case NotifyCollectionChangedAction.Remove: // если удаление
                    {
                        GameWorld.Instance.SendNotification(new NotificationEventArgs($"Завершена игровая сессия"));
                        break;
                    }
                case NotifyCollectionChangedAction.Replace: // если замена

                    break;
            }
        }
    }
}
