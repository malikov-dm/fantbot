﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Factories
{
    public abstract class InventoryItemAbstractFactory<T> where T: InventoryItem
    {
        public abstract T Create(string name, InventoryItemType inventoryItemType);
        public abstract T CreateWeapon(string name, WeaponType weaponType, Buff buff = null);
        public abstract T CreatePistol(string name, Buff buff = null);
        public abstract T CreateConsumableItem(string name, ConsumableItemType consumableItemType);
    }

    public class InventoryItemFactory : InventoryItemAbstractFactory<InventoryItem>
    {
        public override InventoryItem Create(string name, InventoryItemType inventoryItemType)
        {
            return InventoryItem.Create(name, inventoryItemType);
        }

        public override InventoryItem CreateWeapon(string name, WeaponType weaponType, Buff buff = null)
        {
            return Weapon.Create(name, weaponType, buff);
        }

        public override InventoryItem CreatePistol(string name, Buff buff = null)
        {
            return Pistol.Create(name, buff);
        }

        public override InventoryItem CreateConsumableItem(string name, ConsumableItemType consumableItemType)
        {
            return ConsumableItem.Create(name, consumableItemType);
        }
    }
}
