﻿using Rdvb.Tg.Fant.Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Factories
{
    public class QuestFactory
    {
        public static Quest Create(string name, string desc = "") => new Quest(name, desc);
    }
}
