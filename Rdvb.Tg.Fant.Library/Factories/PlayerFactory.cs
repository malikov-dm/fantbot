﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Factories
{
    public class PlayerFactory
    {
        [Obsolete]
        public static Player GetPlayer(GameWorld gameWorld, string playerName, int str, int dex, SpecialAbility specialAbility = SpecialAbility.SecretHit)
        {
            var player = new Player(playerName, str, dex, specialAbility)
            {
                CurrentSection = gameWorld.Sections.FirstOrDefault(x => x.Id == 1)
            };
            return player;
        }

        public static Player CreatePlayer(string playerName, int str, int dex, SpecialAbility specialAbility = SpecialAbility.SecretHit)
        {
            var player = new Player(playerName, str, dex, specialAbility);

            return player;
        }
    }
}
