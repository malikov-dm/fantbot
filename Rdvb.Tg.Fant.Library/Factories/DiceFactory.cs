﻿using Rdvb.Tg.Fant.Library.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Factories
{
    public class DiceFactory
    {
        public static Dice GetDice()
        {
            return new Dice();
        }
    }
}
