﻿using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Factories
{
    public static class GameSessionFactory
    {
        public static GameSession GetGameSession(GameWorld gameWorld, Player player)
        {
            
            var gs = new GameSession(gameWorld, player);
            return gs;
        }

        public static GameSession CreateGameSession(Player player)
        {

            var gs = new GameSession(player);
            return gs;
        }

    }
}
