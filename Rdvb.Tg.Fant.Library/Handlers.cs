﻿using Rdvb.Tg.Fant.Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library
{

    //public delegate void SectionStateHandler(object sender, SectionEventArgs e);
    public delegate void NotificationEventHandler(object sender, NotificationEventArgs e);
    public delegate void UnitStateHandler(object sender, UnitEventArgs e);
    public delegate void DiceEventHandler(object sender, DiceEventArgs e);
    public delegate void SectionEventHandler(object sender, SectionEventArgs e);
    public delegate void UnitEventHandler(object sender, UnitEventArgs e);
    public delegate void InventoryItemEventHandler(object sender, InventoryItemEventArgs e);

    public class DiceEventArgs
    {
        public string Message { get; }

        public DiceEventArgs(string _mes)
        {
            Message = _mes;
        }
    }

    public class SectionEventArgs
    {
        // Сообщение
        public string Message { get; }
        public Link Link { get; }
        public Section Section { get; }
        public Player Player { get; }
        public SectionEventArgs(string _mes, Player _player = null, Link _link = null, Section _section = null)
        {
            Message = _mes;
            Link = _link;
            Section = _section;
            Player = _player;
        }

    }

    public class UnitEventArgs
    {
        // Сообщение
        public string Message { get; }

        public UnitEventArgs(string _mes)
        {
            Message = _mes;
        }

    }

    public class InventoryItemEventArgs
    {
        // Сообщение
        public string Message { get; }

        public InventoryItemEventArgs(string _mes)
        {
            Message = _mes;
        }

    }

    public class NotificationEventArgs : EventArgs
    {
        public string Message { get; }

        public NotificationEventArgs(string _mes)
        {
            Message = _mes;
        }
    }
}
