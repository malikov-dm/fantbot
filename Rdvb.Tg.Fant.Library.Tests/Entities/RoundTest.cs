﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Factories;
using System;
using System.Linq;
namespace Rdvb.Tg.Fant.Library.Tests.Entities
{
    [TestClass]
    public class RoundTest
    {
        [TestMethod]
        public void TestRounds()
        {
            var gw = GameWorldFactory.CreateGameWorldForTests(@"input\RoundTest");
            var player = PlayerFactory.CreatePlayer("ИГРОК ОДИН", 20, 8);
            gw.Player = player;
            var section = gw.GetSectionById(1);
            player.Section = section;

            //player.Section
            //    .MoveNext(5, (p) => { Assert.AreEqual(20, p.Stats.Str); })
            //    .MoveNext(1, (p) => { Assert.AreEqual(20, p.Stats.Str); });

            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(1);

            session.Player.LeaveSection(5);
            session.Player.EnterSection(5);

            //на секции враг и у нас есть заряженные пистолеты. Поэтому выбираем ничего не делать (3 опция)
            session.Player.LeaveSection("3");
            session.Player.EnterSection(5);

            session.Player.LeaveSection(1);
        }
    }
}
