﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rdvb.Tg.Fant.Library.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Tests.Entities
{
    [TestClass]
    public class FaithTest
    {
        [TestMethod]
        public void TestFaithChanges()
        {
            var gw = GameWorldFactory.CreateGameWorldForTests(@"input\FaithTest");
            var player = PlayerFactory.GetPlayer(gw, "ИГРОК ОДИН", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(1);
            session.Player.LeaveSection(2);

            session.Player.EnterSection(2);
            session.Player.LeaveSection(3);
            session.Player.EnterSection(3);
        }
    }
}
