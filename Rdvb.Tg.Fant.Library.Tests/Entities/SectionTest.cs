﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rdvb.Tg.Fant.Library.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rdvb.Tg.Fant.Library.Tests.Entities
{
    [TestClass]
    public class SectionTest
    {
        [TestMethod]
        public void TestGetFoodForMoneyLink()
        {
            var gw = GameWorldFactory.CreateGameWorldForTests("input");
            var player = PlayerFactory.GetPlayer(gw, "ИГРОК ОДИН", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(1);
            session.Player.LeaveSection(2);
            Assert.AreEqual(12, session.Player.Inventory.Money);
            Assert.AreEqual(17, session.Player.Str);

            session.Player.EnterSection(2);
            session.Player.LeaveSection(2);
            Assert.AreEqual(11, session.Player.Inventory.Money);
            Assert.AreEqual(19, session.Player.Str);

            session.Player.EnterSection(2);
            session.Player.LeaveSection(1);
            Assert.AreEqual(11, session.Player.Inventory.Money);
            Assert.AreEqual(19, session.Player.Str);

            session.Player.EnterSection(1);
            Assert.AreEqual(11, session.Player.Inventory.Money);
            Assert.AreEqual(19, session.Player.Str);
        }

        [TestMethod]
        public void TestGetFoodForMoneyLink2()
        {
            var gw = GameWorldFactory.CreateGameWorldForTests("input");
            var player = PlayerFactory.GetPlayer(gw, "ИГРОК ОДИН", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(1);
            session.Player.LeaveSection(2);
            Assert.AreEqual(10, session.Player.Inventory.Money);
            Assert.AreEqual(20, session.Player.Str);
            Assert.AreEqual(false, session.Player.Full);
            Assert.AreEqual(0, session.Player.TravelDayCounter.TotalDay);

            session.Player.EnterSection(2);
            session.Player.LeaveSection(1);
            Assert.AreEqual(10, session.Player.Inventory.Money);
            Assert.AreEqual(13, session.Player.Str);
            Assert.AreEqual(false, session.Player.Full);
            Assert.AreEqual(1, session.Player.TravelDayCounter.TotalDay);

            session.Player.EnterSection(1);
            session.Player.LeaveSection(3);

            session.Player.EnterSection(3);
            Assert.AreEqual(10, session.Player.Inventory.Money);
            Assert.AreEqual(7, session.Player.Str);
            Assert.AreEqual(false, session.Player.Full);
            Assert.AreEqual(2, session.Player.TravelDayCounter.TotalDay);

            session.Player.LeaveSection(3);
            session.Player.EnterSection(3);
            Assert.AreEqual(5, session.Player.Inventory.Money);
            Assert.AreEqual(9, session.Player.Str);
            Assert.AreEqual(true, session.Player.Full);
            Assert.AreEqual(2, session.Player.TravelDayCounter.TotalDay);


        }

        [TestMethod]
        public void TestGetFoodForMoneyLink3()
        {
            var gw = GameWorldFactory.CreateGameWorldForTests("input");
            var player = PlayerFactory.GetPlayer(gw, "ИГРОК ОДИН", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);
        }

        [TestMethod]
        public void TestNoLinksSection()
        {
            var gw = GameWorldFactory.CreateGameWorldForTests(@"input\SectionTest");
            var player = PlayerFactory.GetPlayer(gw, "ИГРОК ОДИН", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(1);
            session.Player.LeaveSection(2);

            var section = session.Player.EnterSection(2);
            Assert.AreEqual(0, section.Links.Count);

        }
    }
}
