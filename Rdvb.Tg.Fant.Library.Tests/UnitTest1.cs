using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rdvb.Tg.Fant.Library;
using Rdvb.Tg.Fant.Library.Factories;
using System;
using System.Linq;

namespace Rdvb.Tg.Fant.Library.Tests
{
    [TestClass]
    public class UnitTest1
    {

        readonly string _filePath = @"input\testbook.json";
        [TestMethod]
        public void TestMethod1()
        {
            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.CreatePlayer("����� ����", 20, 8);
            gw.Player = player;
            var section = gw.GetSectionById(1);
            player.Section = section;

            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            Assert.AreEqual(gw.GameSessions.Count, 1);

            player.Section
                .MoveNext(2, (p) =>
                    {
                        Assert.AreEqual(16, p.Str);
                        Assert.AreEqual(1, p.TravelDayCounter.TotalDay);
                        Assert.AreEqual(4, p.Honor);
                    })
                .MoveNext("4", (p) =>
                    {
                        Assert.AreEqual(9, p.Inventory.Money);
                        Assert.AreEqual(19, p.Str);
                    });


            //session.Player.EnterSection(1);
            //session.Player.LeaveSection(2);
            //Assert.AreEqual(8, session.Player.Inventory.Money);
            //Assert.AreEqual(20, session.Player.Str);

            //session.Player.EnterSection(2);
            //Assert.AreEqual(16, session.Player.Str);
            //Assert.AreEqual(1, session.Player.TravelDayCounter.TotalDay);
            //Assert.AreEqual(4, session.Player.Honor);

            //session.Player.LeaveSection(1);
            //session.Player.EnterSection(1);

            //session.Player.LeaveSection(3);
            //Assert.AreEqual(7, session.Player.Inventory.Money);
            //Assert.AreEqual(3, session.Player.Inventory.Food);

            //session.Player.EnterSection(1);
        }
        [TestMethod]
        public void TestPlayerDead()
        {

            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.CreatePlayer("����� ����", 5, 8);
            gw.Player = player;
            var section = gw.GetSectionById(1);
            player.Section = section;

            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            player.Section
                .MoveNext(7)
                .MoveNext(7, (p) =>
                {
                    Assert.AreEqual(1, p.Inventory.Food);
                    Assert.AreEqual(5, p.Str);
                })
                .MoveNext(1)
                .MoveNext(7)
                .MoveNext(7, (p) =>
                {
                    Assert.AreEqual(0, p.Inventory.Food);
                    Assert.AreEqual(5, p.Str);
                })
                .MoveNext(1)
                .MoveNext(7)
                .MoveNext(1)
                .MoveNext(7, (p) =>
                {
                    Assert.AreEqual(0, p.Inventory.Food);
                    Assert.AreEqual(5, p.Str);
                });


            //session.Player.EnterSection(7);
            //session.Player.LeaveSection(7);
            //session.Player.EnterSection(7);
            //Assert.AreEqual(1, session.Player.Inventory.Food);
            //Assert.AreEqual(5, session.Player.Str);

            //session.Player.LeaveSection(1);
            //session.Player.EnterSection(1);

            //session.Player.LeaveSection(7);
            //session.Player.EnterSection(7);
            //session.Player.LeaveSection(7);
            //session.Player.EnterSection(7);
            //Assert.AreEqual(0, session.Player.Inventory.Food);
            //Assert.AreEqual(5, session.Player.Str);

            //session.Player.LeaveSection(1);
            //session.Player.EnterSection(1);

            //session.Player.LeaveSection(7);
            //session.Player.EnterSection(7);

            //session.Player.LeaveSection(1);
            //session.Player.EnterSection(1);

            //session.Player.LeaveSection(7);
            //session.Player.EnterSection(7);

            //Assert.AreEqual(0, session.Player.Inventory.Food);
            //Assert.AreEqual(0, session.Player.Str);

        }

        [TestMethod]
        public void TestPlayerNoHonor()
        {

            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.GetPlayer(gw, "����� ����", 5, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(8);
            Assert.AreEqual(3, session.Player.Honor);

            session.Player.LeaveSection(1);
            session.Player.EnterSection(1);
            Assert.AreEqual(2, session.Player.Honor);

            session.Player.LeaveSection(8);
            session.Player.EnterSection(8);
            Assert.AreEqual(2, session.Player.Honor);

            session.Player.LeaveSection(1);
            session.Player.EnterSection(1);
            Assert.AreEqual(1, session.Player.Honor);

            session.Player.LeaveSection(8);
            session.Player.EnterSection(8);
            Assert.AreEqual(1, session.Player.Honor);

            session.Player.LeaveSection(1);
            session.Player.EnterSection(1);
            Assert.AreEqual(0, session.Player.Honor);

            session.Player.LeaveSection(8);
            session.Player.EnterSection(8);
            Assert.AreEqual(0, session.Player.Honor);

        }
        [TestMethod]
        public void TestPlayerWithdrawMoney()
        {
            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.GetPlayer(gw, "����� ����", 5, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(10);
            Assert.AreEqual(4, session.Player.Inventory.Money);

            session.Player.LeaveSection(1);
            session.Player.EnterSection(1);
            Assert.AreEqual(4, session.Player.Inventory.Money);

            session.Player.LeaveSection(10);
            session.Player.EnterSection(10);
            Assert.AreEqual(0, session.Player.Inventory.Money);

        }
        [TestMethod]
        public void TestSwimmingAbility()
        {

            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.GetPlayer(gw, "����� ����", 5, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(10);

            session.Player.LeaveSection(5);
            Assert.AreEqual("noswimming", session.Player.TargetSection.SectionId);
        }
        [TestMethod]
        public void TestLeaveSectionByLink()
        {
            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.GetPlayer(gw, "����� ����", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(1);

            session.Player.LeaveSection(2);

            Assert.AreEqual(8, session.Player.Inventory.Money);
            Assert.AreEqual(20, session.Player.Str);


        }

        [TestMethod]
        public void TestSectionEvent()
        {
            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.GetPlayer(gw, "����� ����", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(2);

            Assert.AreEqual(10, session.Player.Inventory.Money);
            Assert.AreEqual(16, session.Player.Str);
            Assert.AreEqual(4, session.Player.Honor);
        }

        [TestMethod]
        public void TestStrOverflow()
        {
            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.GetPlayer(gw, "����� ����", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(2);
            Assert.AreEqual(16, session.Player.Str);

            session.Player.LeaveSection(4);
            Assert.AreEqual(16, session.Player.Str);

            session.Player.EnterSection(4);
            Assert.AreEqual(19, session.Player.Str);

            session.Player.LeaveSection(1);
            Assert.AreEqual(19, session.Player.Str);

            session.Player.EnterSection(1);
            Assert.AreEqual(19, session.Player.Str);

            session.Player.LeaveSection(4);
            Assert.AreEqual(19, session.Player.Str);

            session.Player.EnterSection(4);
            Assert.AreEqual(20, session.Player.Str);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestIncorrectLink()
        {
            var gw = GameWorldFactory.CreateGameWorld(_filePath);
            var player = PlayerFactory.GetPlayer(gw, "����� ����", 20, 10);
            var session = GameSessionFactory.GetGameSession(gw, player);
            gw.GameSessions.Add(session);

            session.Player.EnterSection(2);
            Assert.AreEqual(16, session.Player.Str);

            session.Player.LeaveSection(5);
        }
    }
}
